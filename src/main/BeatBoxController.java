package main;

public class BeatBoxController {
	private BeatBox box;
	private BeatBoxView boxView;
	
	public BeatBoxController(BeatBox box, BeatBoxView boxView){
		this.box = box;
		this.boxView = boxView;
	}
	
	public void buildGUI(BeatBoxView boxView){}
	
	public void startEvent(){
		
	}
	
	public void stopEvent(){
		
	}
	
	public void tempoUpEvent(){
		
	}
	
	public void tempoDownEvent(){

	}
}
