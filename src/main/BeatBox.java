package main;

public class BeatBox {
	private String[] labelNames = {"Bass Drum", "Closed Hi-Hat", "Open Hi-Hat", "Acoustic Snare",
			   "Crash Symbal", "Hand Clap", "High Tom", "Hi Bongo",
			   "Maracas", "Low Conga", "Cowbell", "Vibraslap", "Low-mid Tom",
			   "High Agogo", "Open Hi Conga"
				};

	private String[] buttonNames = {"Start","Stop","Tempo Up","Tempo Down","Send It"};
	
	public BeatBox(){}
	
	public String[] getLabelNames(){
		return labelNames;
	}
	
	public String[] getButtonNames(){
		return buttonNames;
	}
}
