package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

public class BeatBoxView {
//	private String[] labelNames;
//	
//	private String[] buttonNames;
	private ArrayList<JCheckBox> checkBoXList;		
	public BeatBoxView(){
	}
	
	//build JFrame, JPanel,JButton, etc...
	public void buildGUI(BeatBox box){
		String[] labelNames = box.getLabelNames();
		String[] buttonNames = box.getButtonNames();
		JFrame mainFrame = new JFrame("Simple BeatBox");
		//set max size for frame
//		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
//		mainFrame.setUndecorated(true);
		mainFrame.setBounds(50,50,300,300);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BorderLayout layout = new BorderLayout();
		JPanel background = new JPanel(layout);
		background.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
		//left Panel - box lay out
		Box left = new Box(BoxLayout.Y_AXIS);
		for(String labelName : labelNames){
			Label label = createLabel(labelName);
			left.add(label);
		}
		
		//right Panel - box layout
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right,BoxLayout.Y_AXIS));
			right = (JPanel)createButtons(buttonNames,right);

		//center Panel - flow layout
		JPanel center = new JPanel();
		GridLayout grid = new GridLayout(16,16);
		grid.setVgap(1);
		grid.setHgap(2);
		center.setLayout(grid);
		for(int i=0;i<16;i++)
			center = (JPanel) createCheckBoxTimes(16,center);
		
		//menu bar
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		JMenuItem newRecord = new JMenuItem("New record");
		JMenuItem saveRecord = new JMenuItem("Save");
		menu.add(newRecord);
		menu.add(saveRecord);
		menu.addSeparator();
		JMenuItem close = new JMenuItem("Close");
		menu.add(close);
		menuBar.add(menu);
		
		//add Panel to Frame
		mainFrame.add(BorderLayout.WEST, left);
		mainFrame.add(BorderLayout.EAST, right);
		mainFrame.add(BorderLayout.CENTER, center);
		mainFrame.setJMenuBar(menuBar);
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	//create Label on name
	public Label createLabel(String name){
		Label label = new Label(name);
		return label;
	}
	
	//creat Checkbox
	public JCheckBox createCheckBox(){
		return new JCheckBox();
	}
	//create check boxes base on a given number
	public JCheckBox[] createCheckBoxTimes(int time){
		JCheckBox[] checkBoxes = new JCheckBox[time];
		for(int i = 0; i<time; i++)
			checkBoxes[i] = createCheckBox();
		return checkBoxes;
	}
	//create check boxes base on a given number, then add it to a given JComponent
	public JComponent createCheckBoxTimes(int time,JComponent component){
		JComponent newComponent = component;
		JCheckBox[] checkBoxes = createCheckBoxTimes(time);
		for(JCheckBox checkBox : checkBoxes)
			newComponent.add(checkBox);
		return newComponent;
	}
	
	//create Button
	public JButton createButton(String name){
		return new JButton(name);
	}
	
	//create Buttons base on a given String
	public JButton[] createButtons(String[] names){
		JButton[] buttons = new JButton[names.length];
		for(int i=0;i<names.length;i++){
			buttons[i] = createButton(names[i]);
		}
		return buttons;
	}
	//creat Buttons base on a given String then add it to a given JComponent
	public JComponent createButtons(String[] names,JComponent component){
		JComponent newComponent = component;
		JButton[] buttons = createButtons(names);
		for(JButton button : buttons)
			newComponent.add(button);
		return newComponent;
	}
	
	public static void main(String[] args){	
//		BeatBoxView test = new BeatBoxView();
//		test.buildGUI();
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				BeatBoxView test = new BeatBoxView();
				test.buildGUI(new BeatBox());
			}
		});
	}
}
